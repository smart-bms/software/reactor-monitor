#ifndef SETTINGSWRAPPER_H
#define SETTINGSWRAPPER_H

#include <QObject>

#include "smart-bms.h"

class SettingsModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int cellCount
               READ cellCount
               WRITE setCellCount
               NOTIFY cellCountChanged)
    Q_PROPERTY(int slimCount
               READ slimCount
               NOTIFY slimCountChanged)
    Q_PROPERTY(int vol0
               READ vol0
               WRITE setVol0
               NOTIFY vol0Changed)
    Q_PROPERTY(int vol20
               READ vol20
               WRITE setVol20
               NOTIFY vol20Changed)
    Q_PROPERTY(int vol40
               READ vol40
               WRITE setVol40
               NOTIFY vol40Changed)
    Q_PROPERTY(int vol60
               READ vol60
               WRITE setVol60
               NOTIFY vol60Changed)
    Q_PROPERTY(int vol80
               READ vol80
               WRITE setVol80
               NOTIFY vol80Changed)
    Q_PROPERTY(int vol100
               READ vol100
               WRITE setVol100
               NOTIFY vol100Changed)
    Q_PROPERTY(int ovp
               READ ovp
               NOTIFY ovpChanged)
    Q_PROPERTY(int ovpRelease
               READ ovpRelease
               NOTIFY ovpReleaseChanged)
    Q_PROPERTY(int uvp
               READ uvp
               NOTIFY uvpChanged)
    Q_PROPERTY(int uvpRelease
               READ uvpRelease
               NOTIFY uvpReleaseChanged)
    Q_PROPERTY(int chgOcp
               READ chgOcp
               NOTIFY chgOcpChanged)
    Q_PROPERTY(int dsgOcp
               READ dsgOcp
               NOTIFY dsgOcpChanged)
    Q_PROPERTY(int dsgOtp
               READ dsgOtp
               NOTIFY dsgOtpChanged)
    Q_PROPERTY(int dsgUtp
               READ dsgUtp
               NOTIFY dsgUtpChanged)
    Q_PROPERTY(bool loaded
               READ loaded
               NOTIFY loadedChanged)

public:
    explicit SettingsModel(QObject *parent = nullptr);

    int capVol(int index) const;
    int slimCapacity(int index) const;
    int slimVitality(int index) const;

    int cellCount() const;
    int slimCount() const;
    int vol0() const;
    int vol20() const;
    int vol40() const;
    int vol60() const;
    int vol80() const;
    int vol100() const;
    int ovp() const;
    int ovpRelease() const;
    int uvp() const;
    int uvpRelease() const;
    int chgOcp() const;
    int dsgOcp() const;
    int dsgOtp() const;
    int dsgUtp() const;
    bool loaded() const;

signals:
    void cellCountChanged(int);
    void slimCountChanged(int);
    void vol0Changed(int);
    void vol20Changed(int);
    void vol40Changed(int);
    void vol60Changed(int);
    void vol80Changed(int);
    void vol100Changed(int);
    void ovpChanged(int);
    void ovpReleaseChanged(int);
    void uvpChanged(int);
    void uvpReleaseChanged(int);
    void chgOcpChanged(int);
    void dsgOcpChanged(int);
    void dsgOtpChanged(int);
    void dsgUtpChanged(int);
    void loadedChanged(bool);

public slots:
    bool load(const QByteArray &bytes);
    void clear();
    void setCellCount(int);
    void setVol0(int);
    void setVol20(int);
    void setVol40(int);
    void setVol60(int);
    void setVol80(int);
    void setVol100(int);

private:
    int _cellCount;
    int _slimCount;
    int _capVol[CAP_VOLEND];
    int _slimCapacity[MAX_SMART_LIMIT_COUNT];
    int _slimVitality[MAX_SMART_LIMIT_COUNT];
    int _vol0;
    int _vol20;
    int _vol40;
    int _vol60;
    int _vol80;
    int _vol100;
    int _ovp;
    int _ovpRelease;
    int _uvp;
    int _uvpRelease;
    int _chgOcp;
    int _dsgOcp;
    int _dsgOtp;
    int _dsgUtp;
    bool _loaded;
};

#endif // SETTINGSWRAPPER_H
