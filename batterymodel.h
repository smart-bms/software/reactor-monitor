#ifndef BATTERYMODEL_H
#define BATTERYMODEL_H

#include <QObject>

class CellListModel;
class SettingsModel;

class BatteryModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int cellCount
               READ cellCount
               WRITE setCellCount
               NOTIFY cellCountChanged)
    Q_PROPERTY(int voltage
               READ voltage
               WRITE setVoltage
               NOTIFY voltageChanged)
    Q_PROPERTY(int minCell
               READ minCell
               WRITE setMinCell
               NOTIFY minCellChanged)
    Q_PROPERTY(int maxCell
               READ maxCell
               WRITE setMaxCell
               NOTIFY maxCellChanged)
    Q_PROPERTY(int temperature
               READ temperature
               WRITE setTemperature
               NOTIFY temperatureChanged)
    Q_PROPERTY(int slimCapacity
               READ slimCapacity
               WRITE setSlimCapacity
               NOTIFY slimCapacityChanged)
    Q_PROPERTY(int slimVitality
               READ slimVitality
               WRITE setSlimVitality
               NOTIFY slimVitalityChanged)
    Q_PROPERTY(int chargingCurrent
               READ chargingCurrent
               WRITE setChargingCurrent
               NOTIFY chargingCurrentChanged)
    Q_PROPERTY(int dischargingCurrent
               READ dischargingCurrent
               WRITE setDischargingCurrent
               NOTIFY dischargingCurrentChanged)

public:
    explicit BatteryModel(SettingsModel *settings);

    CellListModel *cellList() const;
    SettingsModel *settings() const;

    int cellCount() const;
    int voltage() const;
    int minCell() const;
    int maxCell() const;
    int temperature() const;
    int slimCapacity() const;
    int slimVitality() const;
    int chargingCurrent() const;
    int dischargingCurrent() const;

    Q_INVOKABLE
    float voltageToLevel(int) const;

    Q_INVOKABLE
    float temperatureToLevel(int) const;

signals:
    void cellCountChanged(int);
    void voltageChanged(int);
    void minCellChanged(int);
    void maxCellChanged(int);
    void temperatureChanged(int);
    void slimCapacityChanged(int);
    void slimVitalityChanged(int);
    void chargingCurrentChanged(int);
    void dischargingCurrentChanged(int);

public slots:
    void updateVoltages(QVector<int>);
    void setCellCount(int);
    void setVoltage(int);
    void setMinCell(int);
    void setMaxCell(int);
    void setTemperature(int);
    void setSlimCapacity(int);
    void setSlimVitality(int);
    void setChargingCurrent(int);
    void setDischargingCurrent(int);

private:
    SettingsModel *_settings;
    CellListModel *_cellList;
    int _cellCount;
    int _voltage;
    int _minCell;
    int _maxCell;
    int _temp0;
    int _slimCap;
    int _slimVit;
    int _chargingCurrent;
    int _dischargingCurrent;
};

#endif // BATTERYMODEL_H
