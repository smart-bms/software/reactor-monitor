/*
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __smart_bms_h
#define __smart_bms_h

#include <stdint.h>

#define MAX_CELL_COUNT                  15
#define MAX_SMART_LIMIT_COUNT           6
#define MAX_THERMISTORS                 4

#define COMMAND_ACK                     10
#define COMMAND_TICK                    11
#define COMMAND_TOCK                    12
#define COMMAND_SYNC_TIME               13
#define COMMAND_LOG                     14
#define COMMAND_LOG_RAW                 15
#define COMMAND_BOOT                    16
#define COMMAND_SHUTDOWN                17
#define COMMAND_VOLTAGE                 20
#define COMMAND_REGISTER                21
#define COMMAND_STATE                   22
#define COMMAND_BALANCING               23
#define COMMAND_BALANCE_CELL            24
#define COMMAND_CURRENT                 25
#define COMMAND_TEMPERATURE             26
#define COMMAND_SETTINGS                30
#define COMMAND_CTRL                    31
#define COMMAND_SMART_LIMIT             32
#define COMMAND_LIMITS                  33
#define COMMAND_QUERY_REGISTER          51
#define COMMAND_QUERY_SETTINGS          54
#define COMMAND_QUERY_LIMITS            55
#define COMMAND_QUERY_SMART_LIMIT       56
#define COMMAND_QUERY_SWITCH_DATA       70
#define COMMAND_SWITCH_DATA             71
#define COMMAND_AT                      80



#define BALANCE_OFF                     0
#define BALANCE_DSG                     1
#define BALANCE_CHG                     2
#define BALANCE_MSK                     3

enum
{
    STATE_CHARGING = 1,
    STATE_DISCHARGING,
    STATE_OVERVOLTAGE,
    STATE_UNDERVOLTAGE,
    STATE_CHARGING_OVERCURRENT,
    STATE_DISCHARGING_OVERCURRENT,
    STATE_OVERTEMPERATURE,
    STATE_UNDERTEMPERATURE,
};

#define SETTING_FLAG_SWITCH             0x0001
#define SETTING_FLAG_LOAD_DETECT        0x0002
#define SETTING_FLAG_DSG_BALANCE        0x0004
#define SETTING_FLAG_CHG_BALANCE        0x0008
#define SETTING_FLAG_LED_EN             0x0010
#define SETTING_FLAG_LED_NUM            0x0020
#define SETTING_FLAG_NTC1               0x0040
#define SETTING_FLAG_NTC2               0x0080
#define SETTING_FLAG_NTC3               0x0100
#define SETTING_FLAG_NTC4               0x0200

#define STATE_CTRL_SET                  1
#define STATE_CTRL_RESET                0

enum
{
    CAP_VOL0,
    CAP_VOL20,
    CAP_VOL40,
    CAP_VOL60,
    CAP_VOL80,
    CAP_VOL100,
    CAP_VOLEND,
};

struct settings_data
{
    uint16_t cell_count;
    uint16_t smart_limit_count;
    uint16_t cell_ovp;
    uint16_t cell_ovp_release;
    uint16_t cell_uvp;
    uint16_t cell_uvp_release;
    int16_t chg_otp;
    int16_t chg_otp_release;
    int16_t chg_utp;
    int16_t chg_utp_release;
    int16_t dsg_otp;
    int16_t dsg_otp_release;
    int16_t dsg_utp;
    int16_t dsg_utp_release;
    uint32_t chg_ocp;
    uint16_t chg_ocp_delay;
    uint32_t dsg_ocp;
    uint16_t dsg_ocp_delay;
    uint16_t flags;
    uint32_t nominal_capacity;
    uint32_t nominal_voltage;
    uint16_t cap_vol[CAP_VOLEND];
    uint16_t cell_map[MAX_CELL_COUNT];
    uint16_t slim_cap[MAX_SMART_LIMIT_COUNT];
    uint16_t slim_vit[MAX_SMART_LIMIT_COUNT];
    uint16_t shunt_resistance;
} __attribute__((packed));

struct limits_data
{
    uint16_t ov_min;
    uint16_t ov_max;
    uint16_t uv_min;
    uint16_t uv_max;
} __attribute__((packed));

#define ERROR_BAD_SHELL     2
#define ERROR_BAD_BAUD      3
#define ERROR_BAD_FET       4
#define ERROR_BAD_RESULT    5
#define ERROR_BAD_LOG       6
#define ERROR_BAD_PIN       7

#endif
