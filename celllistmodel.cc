#include "celllistmodel.h"
#include "batterymodel.h"

#include <QDebug>

CellListModel::CellListModel(BatteryModel *battery)
    : QAbstractListModel(battery)
    , _battery(battery)
    , _count(0)
{
}

int CellListModel::count() const
{
    return _count;
}

BatteryModel *CellListModel::battery() const
{
    return _battery;
}

int CellListModel::voltage(int index) const
{
    if (index < _voltages.length())
        return _voltages[index];

    return 0;
}

float CellListModel::level(int index) const
{
    return battery()->voltageToLevel(voltage(index));
}

QHash<int, QByteArray> CellListModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[NumberRole] = "number";
    roles[VoltageRole] = "voltage";
    roles[LevelRole] = "level";

    return roles;
}

int CellListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return count();
}

QVariant CellListModel::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
    case NumberRole:
        return QVariant(index.row() + 1);

    case VoltageRole:
        return QVariant(voltage(index.row()));

    case LevelRole:
        return QVariant(level(index.row()));
    }

    return QVariant::Invalid;
}

bool CellListModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    endInsertRows();
    return true;
}

bool CellListModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    endRemoveRows();
    return true;
}

void CellListModel::updateVoltages(QVector<int> voltages)
{
    _voltages = voltages;
    dataChanged(index(0), index(count() - 1));
}

void CellListModel::setCount(int value)
{
    if (value != count())
    {
        if (value > count())
            insertRows(count(), value - count(), QModelIndex());
        else
            removeRows(value, count() - value, QModelIndex());

        _count = value;
        countChanged(value);
    }
}
