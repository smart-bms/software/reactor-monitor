#ifndef CELLLISTMODEL_H
#define CELLLISTMODEL_H

#include <QObject>
#include <QAbstractListModel>

class BatteryModel;

class CellListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count
               READ count
               WRITE setCount
               NOTIFY countChanged)
public:
    enum CellRole
    {
        NumberRole = Qt::UserRole + 1,
        VoltageRole,
        LevelRole,
    };

    explicit CellListModel(BatteryModel *battery);

    int count() const;
    BatteryModel *battery() const;
    int voltage(int index) const;
    float level(int index) const;

    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool insertRows(int row, int count, const QModelIndex &parent) override;
    bool removeRows(int row, int count, const QModelIndex &parent) override;

signals:
    void countChanged(int);

public slots:
    void updateVoltages(QVector<int>);
    void setCount(int);

private:
    BatteryModel *_battery;
    QVector<int> _voltages;
    int _count;
};

#endif // CELLLISTMODEL_H
