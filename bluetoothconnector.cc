#include "bluetoothconnector.h"

#include <QTimer>
#include <QBluetoothAddress>
#include <QDebug>

namespace
{
    auto SERVICE_UUID = QBluetoothUuid(static_cast<quint16>(0xffe0));
    auto CHARACTERISTIC_UUID = QBluetoothUuid(static_cast<quint16>(0xffe1));
    auto DESCRIPTOR_UUID = QBluetoothUuid(static_cast<quint16>(0x2902));
}

BluetoothConnector::BluetoothConnector(QObject *parent)
    : QObject(parent)
    , _connecting(false)
    , _connected(false)
    , _scanEnabled(false)
    , _reconnect(false)
    , _discoveryAgent(new QBluetoothDeviceDiscoveryAgent(this))
    , _leController(nullptr)
    , _leService(nullptr)
{
    _timer.setInterval(4000);
    _discoveryAgent->setLowEnergyDiscoveryTimeout(4000);

    connect(&_timer, &QTimer::timeout,
            this, &BluetoothConnector::connectingTimeout);
    connect(_discoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered,
            this, &BluetoothConnector::remoteDeviceFound);
    connect(_discoveryAgent, &QBluetoothDeviceDiscoveryAgent::finished,
            this, &BluetoothConnector::scanFinished);
}

BluetoothConnector::~BluetoothConnector()
{
    disconnect();
}

bool BluetoothConnector::connecting()
{
    return _connecting;
}

bool BluetoothConnector::connected()
{
    return _connected;
}

bool BluetoothConnector::scanEnabled()
{
    return _scanEnabled;
}

bool BluetoothConnector::reconnect()
{
    return _reconnect;
}

QString BluetoothConnector::deviceName()
{
    return _deviceName;
}

quint64 BluetoothConnector::deviceAddress()
{
    return _deviceAddress;
}

QString BluetoothConnector::foundDeviceName()
{
    return _foundDeviceName;
}

quint64 BluetoothConnector::foundDeviceAddress()
{
    return _foundDeviceAddress;
}

void BluetoothConnector::setScanEnabled(bool value)
{
    if (value != _scanEnabled)
    {
        _scanEnabled = value;
        scanEnabledChanged(value);

        if (value)
        {
            deviceListCleared();
            _infoMap.clear();
            _discoveryAgent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);
        }
        else
        {
            _discoveryAgent->stop();
        }
    }
}

void BluetoothConnector::setReconnect(bool value)
{
    if (value != _reconnect)
    {
        _reconnect = value;
        reconnectChanged(value);
    }
}

void BluetoothConnector::connectAddress(quint64 address)
{
    if (connected() && address == _deviceAddress)
        return;

    disconnect();

    auto info = _infoMap[address];

    _deviceName = info.name();
    _deviceAddress = address;
    deviceNameChanged(_deviceName);
    deviceAddressChanged(address);

    _leController = QLowEnergyController::createCentral(info, this);

    if (_leController != nullptr)
    {
        connect(_leController, &QLowEnergyController::connected,
                _leController, &QLowEnergyController::discoverServices);
        connect(_leController, &QLowEnergyController::disconnected,
                this, &BluetoothConnector::disconnect);
        connect(_leController, &QLowEnergyController::discoveryFinished,
                this, &BluetoothConnector::serviceDiscoveryFinished);
        connect(_leController, QOverload<QLowEnergyController::Error>::of(&QLowEnergyController::error),
                this, &BluetoothConnector::leError);

        _connecting = true;
        connectingChanged(true);
        deviceConnecting();

        _leController->connectToDevice();
        _timer.setSingleShot(true);
        _timer.start();
    }
}

void BluetoothConnector::disconnect()
{
    auto prevConnected = connected();
    auto prevConnecting = connecting();

    _connected = false;
    _connecting = false;

    if (_leService != nullptr)
    {
        if (_leDescriptor.isValid())
            _leService->writeDescriptor(_leDescriptor, QByteArray::fromHex("0000"));

        delete _leService;
        _leService = nullptr;
    }

    if (_leController != nullptr)
    {
        _leController->disconnectFromDevice();

        delete _leController;
        _leController = nullptr;
    }

    if (prevConnected)
    {
        connectedChanged(false);
        deviceDisconnected();
    }

    if (prevConnecting)
        connectingChanged(false);

    if (prevConnected || prevConnecting)
        deviceDisconnected();

    setScanEnabled(false);

    if (reconnect())
        setScanEnabled(true);
}

void BluetoothConnector::disconnectByUser()
{
    setReconnect(false);
    disconnect();
}

bool BluetoothConnector::writeBytes(QByteArray bytes)
{
    if (_leService != nullptr and _leCharacteristic.isValid())
    {
        _leService->writeCharacteristic(_leCharacteristic, bytes);
        return true;
    }

    return false;
}

QString BluetoothConnector::addressToString(quint64 address)
{
    return QBluetoothAddress(address).toString();
}

void BluetoothConnector::remoteDeviceFound(const QBluetoothDeviceInfo &info)
{
    if ((info.coreConfigurations() & QBluetoothDeviceInfo::LowEnergyCoreConfiguration) and
            (info.name().startsWith("BG-")))
    {
        _infoMap[info.address().toUInt64()] = info;
        _foundDeviceName = info.name();
        _foundDeviceAddress = info.address().toUInt64();

        deviceAdded();

        if (reconnect() and info.address().toUInt64() == deviceAddress())
        {
            setReconnect(false);
            connectAddress(deviceAddress());
        }
    }
}

void BluetoothConnector::scanFinished()
{
    setScanEnabled(false);
}

void BluetoothConnector::serviceDiscoveryFinished()
{
    _leService = _leController->createServiceObject(SERVICE_UUID, this);

    if (_leService != nullptr)
    {
        connect(_leService, &QLowEnergyService::characteristicChanged,
                this, &BluetoothConnector::readCharacteristic);
        connect(_leService, &QLowEnergyService::stateChanged,
                this, &BluetoothConnector::serviceStateChanged);

        _leService->discoverDetails();
    }
    else
    {
        disconnect();
    }
}

void BluetoothConnector::leError(const QLowEnergyController::Error &error)
{
    qDebug() << "LE ERROR " << error;
    disconnect();
}

void BluetoothConnector::serviceStateChanged(const QLowEnergyService::ServiceState &state)
{
    switch (state)
    {
    case QLowEnergyService::ServiceState::ServiceDiscovered:
        _leCharacteristic = _leService->characteristic(CHARACTERISTIC_UUID);

        if (_leCharacteristic.isValid())
        {
            _leDescriptor = _leCharacteristic.descriptor(DESCRIPTOR_UUID);

            if (_leDescriptor.isValid())
            {
                _leService->writeDescriptor(_leDescriptor, QByteArray::fromHex("0100"));
                deviceFullyConnected();
                break;
            }
        }

        disconnect();
        break;

    default:
        break;
    }
}

void BluetoothConnector::deviceFullyConnected()
{
    if (connecting())
    {
        _connecting = false;
        connectingChanged(false);
    }

    if (not connected())
    {
        setReconnect(true);
        _connected = true;
        connectedChanged(true);
        deviceConnected();
    }
}

void BluetoothConnector::readCharacteristic(const QLowEnergyCharacteristic &characteristic,
                                            const QByteArray &bytes)
{
    Q_UNUSED(characteristic)
    readBytes(bytes);
}

void BluetoothConnector::connectingTimeout()
{
    if (connecting())
        disconnect();
}
