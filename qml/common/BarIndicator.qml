import QtQuick 2.0
import QtQuick.Controls 2.0
import ".."

Item {
    id: root

    property string label: "label"
    property string unit: "UNIT"
    property color colorBar: "red"
    property color colorBackground: "gray"
    property real value: 0
    property real maxValue: 10
    property real minValue: 0
    property real level: (value - minValue) / (maxValue - minValue)
    property real barWidth: uiBarWidth * 2
    property bool barOnStart: false

    property real vtrans: 0
    property real htrans: 1

    onLevelChanged: console.log("level: ", level)

    Item {
        property int vX: barOnStart ? barWidth : 0
        property int vWidth: root.width - barWidth
        property int vHeight: root.height
        property int hY: barOnStart ? barWidth : 0
        property int hWidth: root.width
        property int hHeight: root.height - barWidth

        property int size: vtrans * labelBar.width + htrans * labelBar.height

        id: labelBar
        x: vtrans * vX
        y: htrans * hY
        width: vtrans * vWidth + htrans * hWidth
        height: vtrans * vHeight + htrans * hHeight

        Item {
            id: maxLabel
            width: labelBar.size
            height: labelBar.size

            ValueText {
                anchors.centerIn: parent
                value: root.maxValue
                precision: 1
                unit: root.unit
                fontSize: uiFontSizeSmall
                color: Qt.darker(brandColor01, 1.5)

            }
        }

        Item {
            property int vY: labelBar.height - labelBar.size
            property int hX: labelBar.width - labelBar.size

            id: minLabel
            x: htrans * hX
            y: vtrans * vY
            width: labelBar.size
            height: labelBar.size

            ValueText {
                anchors.centerIn: parent
                value: root.minValue
                precision: 1
                unit: root.unit
                fontSize: uiFontSizeSmall
                color: Qt.darker(brandColor01, 1.5)
            }
        }

        Item {
            property int vY: labelBar.height * 0.65 - labelBar.size / 2
            property int hX: labelBar.width * 0.65 - labelBar.size / 2

            id: valueLabel
            x: htrans * hX
            y: vtrans * vY
            width: labelBar.size
            height: labelBar.size

            ValueText {
                anchors.centerIn: parent
                value: root.value
                precision: 1
                unit: root.unit
                fontSize: uiFontSizeSmall
                color: brandColor01
            }
        }

        Item {
            property int vY: labelBar.height * 0.35 - labelBar.size / 2
            property int hX: labelBar.width * 0.35 - labelBar.size / 2

            x: htrans * hX
            y: vtrans * vY
            width: labelBar.size
            height: labelBar.size

            Text {
                anchors.centerIn: parent
                text: root.label
                font.pixelSize: uiFontSizeSmall
                color: brandColor01
            }
        }
    }

    Item {
        property int vX: barOnStart ? 0 : root.width - barWidth
        property int vWidth: barWidth
        property int vHeight: root.height
        property int hY: barOnStart ? 0 : root.height - barWidth
        property int hWidth: root.width
        property int hHeight: barWidth

        property int size: Math.min(width, height)

        id: levelBar
        x: vtrans * vX
        y: htrans * hY
        width: vtrans * vWidth + htrans * hWidth
        height: vtrans * vHeight + htrans * hHeight

        Item {
            id: shadowRect
            width: parent.width
            height: parent.height

            Rectangle {
                anchors.fill: parent
                anchors.margins: (root.barWidth - uiBarWidth) / 2
                radius: uiBarWidth * 0.25
                color: root.colorBackground
            }
        }

        Item {
            property int vX: levelBar.width / 2 - root.barWidth / 2
            property int vY: levelBar.height - levelRect.height
            property int vWidth: root.barWidth
            property int vHeight: levelBar.height * root.level
            property int hX: levelBar.width - levelRect.width
            property int hY: 0
            property int hWidth: levelBar.width * root.level
            property int hHeight: levelBar.height

            id: levelRect
            x: vtrans * vX + htrans * hX
            y: vtrans * vY + htrans * hY
            width: vtrans * vWidth + htrans * hWidth
            height: vtrans * vHeight + htrans * hHeight

            Rectangle {
                anchors.fill: parent
                anchors.margins: (root.barWidth - uiBarWidth) / 2
                radius: uiBarWidth * 0.25
                color: root.colorBar
            }
        }
    }
}

