﻿import QtQuick 2.0

Item {
    id: root

    property int size: Math.min(width, height)
    property int minValue: 30
    property int maxValue: 50
    property real value: 40
    property real arcBegin: 0
    property real arcEnd: 270
    property real arcRotation: 135
    property real level: 0.5
    property real lineWidth: 20
    property real radius: (backgroundCircle.width / 2) - root.lineWidth / 2
    property string label: "label"
    property string unit: "V"
    property string colorCircle: "blue"
    property string colorBackground: "black"
    property bool paintReverse: false
    property bool drawBackground: true

    onLevelChanged: {
        circle.requestPaint()
    }

    Item {
        anchors.centerIn: parent
        width: parent.size
        height: parent.size

        Canvas {
            id: backgroundCircle
            anchors.fill: parent
            anchors.margins: root.size / 20
            rotation: arcRotation
            Component.onCompleted: requestPaint()
            visible: root.drawBackground
            onPaint: {
                var ctx = backgroundCircle.getContext("2d")
                var x = width / 2
                var y = height / 2
                var start = Math.PI * (root.arcBegin / 180)
                var end = Math.PI * (root.arcEnd / 180)
                ctx.reset()
                ctx.lineWidth = root.lineWidth
                ctx.strokeStyle = root.colorBackground
                ctx.beginPath()
                ctx.arc(x, y, root.radius, start, end, false)
                ctx.stroke()
            }
        }

        Canvas {
            id: circle
            anchors.fill: parent
            anchors.margins: backgroundCircle.anchors.margins
            rotation: arcRotation
            Component.onCompleted: requestPaint()
            onPaint: {
                var ctx = circle.getContext("2d")
                var x = width / 2
                var y = height / 2
                var start = Math.PI * (root.arcBegin / 180)
                var end = root.level * Math.PI * (root.arcEnd / 180)
                ctx.reset()
                ctx.lineWidth = root.lineWidth
                ctx.strokeStyle = root.colorCircle
                ctx.beginPath()
                if(root.paintReverse){
                    ctx.arc(x, y, root.radius, Math.PI * (root.arcEnd / 180), end, true)
                }
                else{
                    ctx.arc(x, y, root.radius, start, end, false)
                }
                ctx.stroke()
            }
        }
   }
}
