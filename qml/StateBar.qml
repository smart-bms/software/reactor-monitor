import QtQuick 2.0
import QtQuick.Controls 2.5

Item {
    property string deviceName: ""
    property bool cancelEnabled: false
    property int cancelSize: height
    property real notConnectedOpacity: 0
    property real connectingOpacity: 0
    property real connectedOpacity: 0

    signal mainAction()
    signal cancelAction()

    id: root
    state: "notconnected"

    states: [
        State {
            name: "notconnected"

            PropertyChanges {
                target: root
                notConnectedOpacity: 1
                connectingOpacity: 0
                connectedOpacity: 0
                cancelEnabled: false
            }
        },
        State {
            name: "connecting"

            PropertyChanges {
                target: root
                notConnectedOpacity: 0
                connectingOpacity: 1
                connectedOpacity: 0
                cancelEnabled: true
            }
        },
        State {
            name: "connected"

            PropertyChanges {
                target: root
                notConnectedOpacity: 0
                connectingOpacity: 0
                connectedOpacity: 1
                cancelEnabled: true
            }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation {
                target: root
                properties: "notConnectedOpacity,connectingOpacity,connectedOpacity"
                easing.type: Easing.InOutQuad
                duration: 500
            }
        }
    ]

    Item {
        property real mainExposure: 0
        property real cancelExposure: 0

        id: cancelAnimator
        anchors.fill: parent
        state: cancelEnabled ? "cancelenabled" : "canceldisabled"

        states: [
            State {
                name: "cancelenabled"
                PropertyChanges {
                    target: cancelAnimator
                    mainExposure: 0
                    cancelExposure: 1
                }
            },
            State {
                name: "canceldisabled"
                PropertyChanges {
                    target: cancelAnimator
                    mainExposure: 1
                    cancelExposure: 0
                }
            }
        ]

        transitions: [
            Transition {
                from: "canceldisabled"
                to: "cancelenabled"
                reversible: true

                SequentialAnimation {
                    NumberAnimation {
                        properties: "mainExposure"
                        duration: 100
                    }
                    NumberAnimation {
                        properties: "cancelExposure"
                        duration: 100
                    }
                }
            }
        ]

        Item {
            id: mainItem
            width: parent.width - (1 - cancelAnimator.mainExposure) * cancelSize
            height: parent.height

            Item {
                id: background
                anchors.fill: parent
                anchors.margins: 5
                opacity: mainMouseArea.pressed ? 0.6 : 1.0

                Rectangle {
                    anchors.fill: parent
                    radius: 5
                    color: brandColor02
                }

                Item {
                    anchors.fill: parent
                    opacity: root.notConnectedOpacity

                    Text {
                        anchors.centerIn: parent
                        font.pointSize: uiFontSizeNormal
                        color: brandColor12
                        text: "No connection"
                    }
                }

                Item {
                    id: connectingView
                    anchors.fill: parent
                    opacity: root.connectingOpacity

                    Row {
                        anchors.centerIn: parent

                        BusyIndicator {
                            anchors.verticalCenter: parent.verticalCenter
                            height: connectingView.height * 0.8
                            width: height
                        }

                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            font.pointSize: uiFontSizeNormal
                            text: "Connecting to " + root.deviceName
                            color: brandColor12
                        }
                    }
                }

                Item {
                    id: connectedView
                    anchors.fill: parent
                    opacity: root.connectedOpacity

                    Text {
                        anchors.centerIn: parent
                        font.pointSize: uiFontSizeNormal
                        text: "Connected with " + root.deviceName
                        color: brandColor12
                    }
                }
            }

            MouseArea {
                id: mainMouseArea
                anchors.fill: parent
                onClicked: root.mainAction()
            }
        }

        Item {
            id: cancelItem
            x: parent.width - width
            width: cancelSize
            height: parent.height

            Rectangle {
                id: cancelButton
                anchors.fill: parent
                anchors.margins: 5
                radius: 5
                color: brandColor02
                opacity: (cancelMouseArea.pressed ? 0.6 : 1.0) * cancelAnimator.cancelExposure

                Canvas {
                    anchors.fill: parent
                    anchors.margins: cancelSize * 0.25
                    Component.onCompleted: requestPaint()
                    onPaint: {
                        var ctx = getContext("2d")
                        ctx.strokeStyle = brandColor12
                        ctx.lineWidth = cancelSize * 0.025
                        ctx.moveTo(0, 0)
                        ctx.lineTo(width, height)
                        ctx.moveTo(width, 0)
                        ctx.lineTo(0, height)
                        ctx.stroke()
                    }
                }
            }

            MouseArea {
                id: cancelMouseArea
                anchors.fill: parent
                onClicked: root.cancelAction()
                enabled: parent.opacity > 0.5
            }
        }
    }
}
