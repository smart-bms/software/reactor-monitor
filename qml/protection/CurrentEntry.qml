import QtQuick 2.0
import "../common"
import "../monitor"

Item {
    ProtectionEntry {
        anchors.fill: parent
        anchors.margins: uiFontSizeNormal
        label: "Current"

        ProtectionTable {
            id: currentTable
            unit: "A"
            x: parent.descX
            y: parent.descY
            width: parent.descWidth
            height: parent.descHeight
            lowerColor: brandColor05
            upperColor: brandColor02
        }

        Item {
            x: parent.circX
            y: parent.circY
            width: parent.circWidth
            height: parent.circHeight

            ValueCircle {
                id: externalCircleTemp
                colorCircle: currentTable.upperColor
                colorBackground: brandColor11
                arcBegin: 0
                arcEnd: 270
                arcRotation: 225
                lineWidth: uiBarWidth
                minValue: 0
                maxValue: settingsModel.dsgOcp
                value: batteryModel.dischargingCurrent
                level: Math.max(Math.min(((value - minValue) / (maxValue - minValue)), 1), 0)
                width: parent.width
                height: parent.height
            }

            ValueCircle {
                id: internalCircleTemp
                colorCircle: currentTable.lowerColor
                colorBackground: brandColor11
                radius: externalCircleTemp.radius - uiBarWidth * 2
                arcBegin: 0
                arcEnd: externalCircleTemp.arcEnd
                arcRotation: externalCircleTemp.arcRotation
                lineWidth: uiBarWidth
                minValue: 0
                maxValue: settingsModel.chgOcp
                value: batteryModel.chargingCurrent
                level: Math.max(Math.min(((value - minValue) / (maxValue - minValue)), 1), 0)
                width: parent.width
                height: parent.height
            }
        }
    }
}
