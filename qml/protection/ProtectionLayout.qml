import QtQuick 2.0
import "../common"
import "../slim"

Item {
    property real vtrans: 0
    property real htrans: 1
    property int entryWidth: uiFontSizeNormal * 20
    property int entryHeight: uiFontSizeNormal * 8

    id: root
    anchors.fill: parent

    VoltageEntry {
        id: voltage

        property int vX: root.width / 2 - voltage.width / 2
        property int vY: root.height / 8 - voltage.height / 2
        property int hX: root.width / 4 - voltage.width / 2
        property int hY: root.height / 4 - voltage.height / 2

        x: root.vtrans * vX + root.htrans * hX
        y: root.vtrans * vY + root.htrans * hY
        width: entryWidth
        height: entryHeight
    }

    CurrentEntry {
        id: current

        property int vX: root.width / 2 - current.width / 2
        property int vY: root.height * 3 / 8 - current.height / 2
        property int hX: root.width / 4 - current.width / 2
        property int hY: root.height * 3 / 4 - current.height / 2

        x: root.vtrans * vX + root.htrans * hX
        y: root.vtrans * vY + root.htrans * hY
        width: entryWidth
        height: entryHeight
    }

    TemperatureEntry {
        id: temperature

        property int vX: root.width / 2 - temperature.width / 2
        property int vY: root.height * 5 / 8 - temperature.height / 2
        property int hX: root.width * 3 / 4 - temperature.width / 2
        property int hY: root.height / 4 - voltage.height / 2

        x: root.vtrans * vX + root.htrans * hX
        y: root.vtrans * vY + root.htrans * hY
        width: entryWidth
        height: entryHeight
    }

    SlimSettings {
        id: capacity

        property int vX: root.width / 2 - capacity.width / 2
        property int vY: root.height * 7 / 8 - capacity.height / 2
        property int hX: root.width * 3 / 4 - capacity.width / 2
        property int hY: root.height * 3 / 4 - capacity.height / 2

        x: root.vtrans * vX + root.htrans * hX
        y: root.vtrans * vY + root.htrans * hY
        width: entryWidth - 2 * uiFontSizeNormal
        height: entryHeight
    }
}
