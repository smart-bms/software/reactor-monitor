import QtQuick 2.0
import "../common"
import "../monitor"

Item {
    id: root

    ProtectionEntry {
        anchors.fill: parent
        anchors.margins: uiFontSizeNormal
        label: "Temperature"

        ProtectionTable {
            id: currentTable
            x: parent.descX
            y: parent.descY
            width: parent.descWidth
            height: parent.descHeight
            unit: "°C"
            upperCutout: settingsModel.dsgOtp
            upperRelease: settingsModel.dsgOtp
            lowerCutout: settingsModel.dsgUtp
            lowerRelease: settingsModel.dsgUtp
            lowerLabel: "LOW"
            upperLabel: "HIGH"
            lowerColor: brandColor04
            upperColor: brandColor05
        }

        Item {
            x: parent.circX
            y: parent.circY
            width: parent.circWidth
            height: parent.circHeight

            Rectangle {
                anchors.centerIn: parent
                width: Math.min(parent.width, parent.height) / 2
                height: Math.min(parent.width, parent.height) / 2
                opacity: 0.05
                radius: parent.height * 0.5
            }

            Item {
                width: valueText.width
                height: valueText.height
                anchors.centerIn: parent

                Text {

                    id: valueText
                    font.pointSize: uiFontSizeNormal
                    text: batteryModel.temperature + currentTable.unit
                    color: brandColor01
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
            }

            ValueCircle {
                id: externalCircleTemp
                colorCircle: currentTable.upperColor
                colorBackground: brandColor11
                arcBegin: 0
                arcEnd: 270
                arcRotation: 225
                lineWidth: uiBarWidth
                minValue: settingsModel.dsgUtp
                maxValue: settingsModel.dsgOtp
                value: batteryModel.temperature
                level: Math.max(Math.min(((value - minValue) / (maxValue - minValue)), 1), 0)
                width: parent.width
                height: parent.height
            }

            ValueCircle {
                id: internalCircleTemp
                colorCircle: currentTable.lowerColor
                colorBackground: externalCircleTemp.colorBackground
                radius: externalCircleTemp.radius
                arcBegin: 0
                arcEnd: externalCircleTemp.arcEnd
                arcRotation: externalCircleTemp.arcRotation
                lineWidth: uiBarWidth
                minValue: externalCircleTemp.minValue
                maxValue: externalCircleTemp.maxValue
                value: batteryModel.temperature
                level: Math.max(Math.min(((value - minValue) / (maxValue - minValue)), 1), 0)
                width: parent.width
                height: parent.height
                paintReverse: true
                drawBackground: false
            }
        }
    }
}
