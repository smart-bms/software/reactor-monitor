import QtQuick 2.0

Item {
    property string label: "label"
    property real descX: 0
    property real descY: height * 0.25
    property real descWidth: width * 0.6
    property real descHeight: height * 0.75
    property real circX: width * 0.6
    property real circY: 0
    property real circWidth: width * 0.4
    property real circHeight: height

    Item {
        width: parent.width * 0.6
        height: parent.height * 0.25

        Text {
            font.pointSize: uiFontSizeBig
            text: label
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: uiMargins
            color: brandColor02
        }
    }
}
