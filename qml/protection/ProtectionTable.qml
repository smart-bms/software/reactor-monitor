import QtQuick 2.5

Item {
    property string upperLabel: "CHG"
    property string lowerLabel: "DSG"
    property string unit: "V"

    property real lowerCutout: 3.2
    property real lowerRelease: 3.3
    property color lowerColor: "yellow"

    property real upperCutout: 4.2
    property real upperRelease: 4.1
    property color upperColor: "green"

    id: root

    Grid {
        property int itemWidth: width / columns
        property int itemHeight: height / rows

        anchors.fill: parent
        id: grid
        columns: 3
        rows: 3
        horizontalItemAlignment: Grid.AlignHCenter
        verticalItemAlignment: Grid.AlignVCenter

        Item {
            width: grid.itemWidth
            height: grid.itemHeight
        }

        Item {
            width: grid.itemWidth
            height: grid.itemHeight

            Text {
                anchors.centerIn: parent
                color: brandColor01
                text: "Cutout"
            }
        }

        Item {
            width: grid.itemWidth
            height: grid.itemHeight

            Text {
                anchors.centerIn: parent
                color: brandColor01
                text: "Release"
            }
        }

        Item {
            width: grid.itemWidth
            height: grid.itemHeight

            Text {
                anchors.centerIn: parent
                color: brandColor01
                text: root.upperLabel
            }
        }

        Item {
            width: grid.itemWidth
            height: grid.itemHeight

            Text {
                anchors.centerIn: parent
                color: root.upperColor
                text: root.upperCutout + root.unit
            }
        }

        Item {
            width: grid.itemWidth
            height: grid.itemHeight

            Text {
                anchors.centerIn: parent
                color: root.upperColor
                text: root.upperRelease + root.unit
            }
        }

        Item {
            width: grid.itemWidth
            height: grid.itemHeight

            Text {
                anchors.centerIn: parent
                color: brandColor01
                text: root.lowerLabel
            }
        }

        Item {
            width: grid.itemWidth
            height: grid.itemHeight

            Text {
                anchors.centerIn: parent
                color: root.lowerColor
                text: root.lowerCutout + root.unit
            }
        }

        Item {
            width: grid.itemWidth
            height: grid.itemHeight

            Text {
                anchors.centerIn: parent
                color: root.lowerColor
                text: root.lowerRelease + root.unit
            }
        }
    }
}
