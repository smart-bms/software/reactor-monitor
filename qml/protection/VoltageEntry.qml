import QtQuick 2.0
import "../common"

Item {
    ProtectionEntry {
        anchors.fill: parent
        anchors.margins: uiFontSizeNormal
        label: "Voltage"

        ProtectionTable {
            id: voltageTable
            x: parent.descX
            y: parent.descY
            width: parent.descWidth
            height: parent.descHeight
            unit: "mV"
            lowerCutout: settingsModel.uvp
            lowerRelease: settingsModel.uvpRelease
            upperCutout: settingsModel.ovp
            upperRelease: settingsModel.ovpRelease
            lowerColor: brandColor05
            upperColor: brandColor02
        }

        Item {
            x: parent.circX
            y: parent.circY
            width: parent.circWidth
            height: parent.circHeight

            ValueCircle {
                id: externalCircle
                colorCircle: voltageTable.upperColor
                colorBackground: brandColor11
                arcBegin: 0
                arcEnd: 270
                arcRotation: 225
                lineWidth: uiBarWidth
                minValue: settingsModel.uvp
                maxValue: settingsModel.ovp
                value: batteryModel.maxCell
                level: Math.max(Math.min(((value - minValue) / (maxValue - minValue)), 1), 0)
                width: parent.width
                height: parent.height
            }

            ValueCircle {
                id: internalCircle
                colorCircle: voltageTable.lowerColor
                colorBackground: brandColor11
                radius: externalCircle.radius - uiBarWidth * 2
                arcBegin: 0
                arcEnd: externalCircle.arcEnd
                arcRotation: externalCircle.arcRotation
                lineWidth: uiBarWidth
                minValue: externalCircle.minValue
                maxValue: externalCircle.maxValue
                value: batteryModel.minCell
                level: Math.max(Math.min(((value - minValue) / (maxValue - minValue)), 1), 0)
                width: parent.width
                height: parent.height
            }
        }
    }
}
