import QtQuick 2.0

Item {
    property real value: 0
    property int precision: 0
    property int fontSize: uiFontSizeNormal
    property string unit: "unit"
    property string color: brandColor02

    id: root

    Text {
        anchors.centerIn: parent
        text: value.toFixed(precision) + unit
        color: root.color
        font.pixelSize: root.fontSize
    }
}
