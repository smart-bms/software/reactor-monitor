import QtQuick 2.0

Rectangle {
    property string name: "name"
    property string address: "address"

    signal clicked()

    id: root
    anchors.fill: parent
    anchors.margins: 5
    radius: 5
    color: brandColor11
    opacity: mouseArea.pressed ? 0.6 : 1.0

    Item {
        anchors.fill: parent
        anchors.margins: uiMargins

        Text {
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            text: root.name
            font.pointSize: uiFontSizeNormal
            color: brandColor02
        }

        Text {
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            text: root.address
            font.pointSize: uiFontSizeNormal
            color: brandColor02
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: root.clicked()
    }
}
