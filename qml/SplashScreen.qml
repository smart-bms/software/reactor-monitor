import QtQuick 2.0

Item {
    id: root
    x: 0
    y: parent.height * swipe
    width: parent.width
    height: parent.height

    property real swipe: 0
    property bool appReady: false
    property bool splashReady: false
    property bool ready: appReady && splashReady

    signal readyToGo()
    signal detached()

    function hide() {
        hideAnimation.running = true
    }

    PropertyAnimation {
        id: hideAnimation
        target: root
        property: "swipe"
        from: 0
        to: 1
        easing.type: Easing.OutBounce
        duration: uiAnimationTimeLong
    }

    onReadyChanged: {
        if (ready)
            readyToGo()
    }

    Item {
        anchors.fill: parent

        Rectangle {
            anchors.fill: parent
            color: brandColor11
        }

        Image {
            anchors.fill: parent
            source: "qrc:/graphics/reactor-energy.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    Timer {
        id: splashTimer
        interval: 500
        onTriggered: splashReady = true
    }

    Component.onCompleted: splashTimer.start()
}
