import QtQuick 2.0
import "protection"
import "monitor"

Item {
    id: root
    anchors.fill: parent

    property int barHeight: connector.topBarHeight
    property int maxBarWidth: barHeight * 8
    property int offset: (height - barHeight) * (1 - connector.exposure / 2)

    state: "presentation"

    states: [
        State {
            name: "presentation"
            PropertyChanges {
                target: connector
                exposure: 0
            }
            PropertyChanges {
                target: bluetoothConnector
                scanEnabled: false
            }
        },
        State {
            name: "connection"
            PropertyChanges {
                target: connector
                exposure: 1
            }
            PropertyChanges {
                target: bluetoothConnector
                scanEnabled: true
            }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation {
                target: connector
                property: "exposure"
                duration: 300
                easing.type: Easing.InOutQuad
            }
        }
    ]

    Image {
        id: background
        width: parent.width * 2
        height: parent.height
        source: "qrc:/graphics/reactor-energy.png"
        fillMode: Image.PreserveAspectCrop
        state: appLayout.screenIndex < 1 ? "left" : "right"
        states: [
            State {
                name: "left"

                PropertyChanges {
                    target: background
                    x: 0
                }
            },
            State {
                name: "right"

                PropertyChanges {
                    target: background
                    x: -background.width / 2
                }
            }
        ]

        transitions: [
            Transition {
                NumberAnimation {
                    properties: "x"
                    duration: uiAnimationTimeLong
                    easing.type: Easing.InOutQuad
                }
            }
        ]

        Rectangle {
            anchors.fill: parent
            color: brandColor12
            opacity: 0.85
        }
    }

    AppLayout {
        id: appLayout
        anchors.fill: parent
        anchors.bottomMargin: root.barHeight
    }

    Rectangle {
        anchors.fill: parent
        color: brandColor12
        opacity: connector.exposure * 0.8
    }

    ScanBar {
        id: scanBar
        anchors.horizontalCenter: parent.horizontalCenter
        width: Math.min(parent.width, maxBarWidth)
        height: root.barHeight
        active: bluetoothConnector.scanEnabled
    }

    Connector {
        id: connector
        anchors.horizontalCenter: parent.horizontalCenter
        y: offset
        width: Math.min(parent.width, maxBarWidth)
        height: parent.height
        onClicked: {
            if (root.state == "presentation") {
                root.state = "connection"
            } else if (root.state == "connection") {
                root.state = "presentation"
            }
        }
    }

    Connections {
        target: bluetoothConnector
        onDeviceConnected: root.state = "presentation"
    }
}
