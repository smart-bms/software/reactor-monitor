import QtQuick 2.0
import "../common"
import ".."

Item {
    property real vtrans: 0
    property real htrans: 1

    id: root

    BarIndicator {
        property int verticalWidth: root.width * 0.15
        property int verticalHeight: root.height * 0.4
        property int horizontalWidth: root.width * 0.4
        property int horizontalHeight: root.height * 0.15

        id: dsgCurrent
        label: "DSG"
        unit: "A"
        maxValue: settingsModel.dsgOcp / 1000
        colorBackground: brandColor11
        colorBar: brandColor02
        value: batteryModel.dischargingCurrent
        width: vtrans * verticalWidth + htrans * horizontalWidth
        height: vtrans * verticalHeight + htrans * horizontalHeight
        barOnStart: true
        vtrans: root.vtrans
        htrans: root.htrans
    }

    ValueCircle {
        property int vX: root.width * 0.15
        property int vY: 0
        property int vWidth: root.width * 0.7
        property int vHeight: root.height * 0.4
        property int hX: 0
        property int hY: root.height * 0.15
        property int hWidth: root.width * 0.4
        property int hHeight: root.height * 0.7

        id: voltage
        colorCircle: brandColor02
        colorBackground: brandColor11
        arcBegin: 0
        arcEnd: 270
        arcRotation: 135
        lineWidth: uiBarWidth
        minValue: settingsModel.vol0
        maxValue: settingsModel.vol100
        value: Math.round(batteryModel.voltage * 100 / 100000)
        level: batteryModel.voltageToLevel(batteryModel.voltage / batteryModel.cellCount)
        x: vtrans * vX + htrans * hX
        y: vtrans * vY + htrans * hY
        width: vtrans * vWidth + htrans * hWidth
        height: vtrans * vHeight + htrans * hHeight

        VoltageLabel {
            size: Math.min(parent.width, parent.height)
            value: batteryModel.voltage / 1000
            maxValue: voltage.maxValue
            minValue: voltage.minValue
            level: voltage.level
            unit: voltage.unit
        }

        Item {
            rotation: -45
            x: voltage.width / 2 - (voltage.radius * Math.cos(55 * Math.PI / 180))
            y: voltage.height / 2 + (voltage.radius * Math.sin(55 * Math.PI / 180))

            ValueText {
                anchors.centerIn: parent
                fontSize: uiFontSizeSmall
                color: Qt.darker(brandColor01, 1.5)
                value: voltage.minValue / 100
                unit: voltage.unit
                precision: 1
            }
        }

        Item {
            rotation: 45
            x: voltage.width / 2 + (voltage.radius * Math.cos(55 * Math.PI / 180))
            y: voltage.height / 2 + (voltage.radius * Math.sin(55 * Math.PI / 180))

            ValueText {
                anchors.centerIn: parent
                fontSize: uiFontSizeSmall
                color: Qt.darker(brandColor01, 1.5)
                value: voltage.maxValue / 100
                unit: voltage.unit
                precision: 1
            }
        }
    }

    BarIndicator {
        property int vX: root.width * 0.85
        property int vY: 0
        property int hX: 0
        property int hY: root.height * 0.85

        id: chgCurrent
        label: "CHG"
        unit: "A"
        maxValue: settingsModel.chgOcp / 10000
        colorBackground: brandColor11
        colorBar: brandColor02
        value: batteryModel.chargingCurrent
        x: vtrans * vX + htrans * hX
        y: vtrans * vY + htrans * hY
        width: dsgCurrent.width
        height: dsgCurrent.height
        barOnStart: false
        vtrans: root.vtrans
        htrans: root.htrans
    }

    CellList {
        property int vX: 0
        property int vY: root.height * 0.4
        property int vWidth: root.width
        property int vHeight: root.height * 0.6
        property int hX: root.width * 0.4
        property int hY: 0
        property int hWidth: root.width * 0.6
        property int hHeight: root.height

        id: cellList
        x: vtrans * vX + htrans * hX
        y: vtrans * vY + htrans * hY
        width: vtrans * vWidth + htrans * hWidth
        height: vtrans * vHeight + htrans * hHeight
    }
}
