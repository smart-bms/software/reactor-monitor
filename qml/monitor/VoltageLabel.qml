import QtQuick 2.0
import ".."

Item {
    property real size: 200
    property real minValue: 30
    property real maxValue: 40
    property string value: "value"
    property string unit: "unit"
    property string level: "level"

    id: root

    width: size / 2
    height: size / 2
    anchors.centerIn: parent

    Rectangle {
        anchors.fill: parent
        color: brandColor11
        radius: parent.height * 0.5
    }

    Item {
        width: voltageValue.width
        height: voltageValue.width
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: parent.height * 0.3

        ValueText {
            id: voltageValue
            value: root.value
            unit: root.unit
            precision: 1
            color: brandColor01
        }
    }

    Item {
        anchors.centerIn: parent
        width: percentText.width
        height: percentText.height

        Text {
            id: percentText
            anchors.fill: parent
            font.pointSize: uiFontSizeNormal
            text: Math.round(level * 10000 / 100) + "%"
            color: brandColor01
            horizontalAlignment: Text.AlignHCenter
        }
    }
}
