import QtQuick 2.0
import QtQuick.Controls 2.0

Item {
    property int entryWidth: width
    property int entryHeight: uiFontSizeNormal * 2
    property int indexWidth: uiFontSizeNormal * 2
    property int voltageWidth: uiFontSizeNormal * 6

    id: root
    state: cellListModel.count > 0 ? "active" : "inactive"

    Component {
        id: cellDelegate

        Item {
            id: entry
            width: root.entryWidth
            height: root.entryHeight
            state: "ready"
            states: [
                State {
                    name: "ready"

                    PropertyChanges {
                        target: entry
                        opacity: 0
                    }
                },
                State {
                    name: "active"

                    PropertyChanges {
                        target: entry
                        opacity: 1
                    }
                }
            ]

            transitions: [
                Transition {
                    NumberAnimation {
                        target: entry
                        properties: "opacity"
                        duration: uiAnimationTimeNormal
                        easing.type: Easing.InOutQuad
                    }
                }
            ]

            Component.onCompleted: state = "active"

            Item {
                anchors.fill: parent
                anchors.margins: 5

                Item {
                    width: root.indexWidth
                    height: parent.height

                    Text {
                        anchors.centerIn: parent
                        text: "" + number
                        color: "white"
                        font.pointSize: uiFontSizeNormal
                    }
                }

                Item {
                    x: root.indexWidth
                    width: parent.width - root.indexWidth - root.voltageWidth
                    height: parent.height

                    Item {
                        anchors.fill: parent
                        anchors.margins: parent.height * 0.25

                        Rectangle {
                            color: brandColor11
                            x: parent.width * level - radius * 2
                            width: parent.width - x + radius * 2
                            height: uiBarWidth
                            radius: uiBarWidth * 0.25
                        }

                        Rectangle {
                            color: brandColor02
                            x: 0
                            width: parent.width * level
                            height: uiBarWidth
                            radius: uiBarWidth * 0.25
                        }
                    }
                }

                Item {
                    width: root.voltageWidth
                    height: parent.height
                    x: parent.width - width

                    Text {
                        anchors.centerIn: parent
                        text: voltage + "mV"
                        color: "white"
                        font.pointSize: uiFontSizeNormal
                    }
                }
            }
        }
    }

    ListView {
        id: list
        anchors.fill: parent
        delegate: cellDelegate
        model: cellListModel
    }
}
