import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Rectangle {

    Column {
        anchors.fill: parent

        Button {
            text: "foobar"
        }

        ProgressBar {
            value: 0.8
        }
    }
}
