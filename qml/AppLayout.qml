import QtQuick 2.0
import "monitor"
import "protection"
import "settings"
import QtQuick.Controls 2.5

Item {
    property real verticalTransition: 0
    property real vtrans: verticalTransition
    property real htrans: 1 - verticalTransition
    property int screenIndex: view.currentIndex

    id: root
    state: width < height ? "vertical" : "horizontal"
    anchors.fill: parent

    states: [
        State {
            name: "vertical"

            PropertyChanges {
                target: root
                verticalTransition: 1
            }
        },
        State {
            name: "horizontal"

            PropertyChanges {
                target: root
                verticalTransition: 0
            }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation {
                properties: "verticalTransition"
                duration: uiAnimationTimeLong
                easing.type: Easing.OutBack
            }
        }
    ]

    SwipeView {
        id: view
        anchors.fill: parent
        currentIndex: 0

        Item {
            MonitorLayout {
                anchors.fill: parent
                anchors.bottomMargin: root.barHeight
                vtrans: root.vtrans
                htrans: root.htrans
            }
        }

        Item {
            ProtectionLayout {
                anchors.fill: parent
                anchors.bottomMargin: root.barHeightr
                vtrans: root.vtrans
                htrans: root.htrans
            }
        }
    }

    PageIndicator {
        id: indicator

        count: view.count
        currentIndex: view.currentIndex

        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
