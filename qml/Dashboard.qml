import QtQuick 2.0
import QtQuick.Shapes 1.12

Item {
    property real voltageLevel: 0.1
    property int bottomMargin: 0
    property int barWidth: width
    property int barHeight: width * 0.2

    Column {
        anchors.fill: parent

        ValueBar {
            width: barWidth
            height: barHeight
            label: "voltage"
            unit: "V"
            color: "red"
            minValue: settingsModel.vol0 * settingsModel.cellCount / 1000
            maxValue: settingsModel.vol100 * settingsModel.cellCount / 1000
            value: batteryModel.voltage / 1000
            level: batteryModel.voltageToLevel(batteryModel.voltage / settingsModel.cellCount)
        }

        ValueBar {
            width: barWidth
            height: barHeight
            label: "temperature"
            unit: "\xB0C"
            color: "yellow"
            minValue: -20
            maxValue: 60
            value: batteryModel.temperature
            level: batteryModel.temperatureToLevel(batteryModel.temperature)
        }

        ValueBar {
            width: barWidth
            height: barHeight
            label: "current"
            unit: "A"
            color: "orange"
            minValue: 0
            maxValue: 50
        }


    }
}
