import QtQuick 2.12
import QtQuick.Controls 2.5

ApplicationWindow {
    id: window
    visible: true
    width: 400
    height: 800
    title: "Reactor Monitor"

    Loader {
        id: appLoader
        anchors.fill: parent
        visible: false
        asynchronous: true

        onStatusChanged: {
            if (status === Loader.Ready) {
                splashLoader.item.appReady = true
            }
        }
    }

    Loader {
        id: splashLoader
        anchors.fill: parent
        source: "SplashScreen.qml"
        visible: true
        asynchronous: false

        onStatusChanged: {
            if (status === Loader.Ready) {
                appLoader.setSource("Slider.qml");
            }
        }
    }

    Connections {
        target: splashLoader.item
        onReadyToGo: {
            appLoader.visible = true
            splashLoader.item.hide()
        }
        onDetached: {
            splashLoader.setSource("")
        }
    }
}
