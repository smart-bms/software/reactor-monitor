import QtQuick 2.12
import QtQuick.Controls 2.5

Item {
    property real topBarHeight: uiFontSizeNormal * 3
    property bool connected: false
    property bool enabled: false
    property real exposure: 0
    property int animationDuration: 400

    signal clicked()

    id: root

    Connections {
        target: bluetoothConnector
        onDeviceAdded: {
            deviceListView.model.append({
                            "deviceName": target.foundDeviceName,
                            "deviceAddress": target.foundDeviceAddress,
                            "deviceAddressString": target.addressToString(target.foundDeviceAddress)
                        })
        }

        onDeviceListCleared: {
            deviceListView.model.clear()
        }

        onDeviceConnecting: {
            stateBar.state = "connecting"
        }

        onDeviceConnected: {
            stateBar.state = "connected"
        }

        onDeviceDisconnected: {
            stateBar.state = "notconnected"
        }
    }

    Column {
        width: parent.width
        height: parent.height

        StateBar {
            id: stateBar
            width: root.width
            height: root.topBarHeight
            deviceName: bluetoothConnector.deviceName
            onMainAction: root.clicked()
            onCancelAction: bluetoothConnector.disconnectByUser()
        }

        Item {
            id: connectionPanel
            width: root.width
            height: root.height - root.topBarHeight

            Column {
                anchors.fill: parent

                Item {
                    width: parent.width
                    height: parent.height - root.topBarHeight

                    Component {
                        id: batteryDelegate

                        Item {
                            width: parent.width
                            height: root.topBarHeight

                            DeviceEntry {
                                name: deviceName
                                address: deviceAddressString
                                onClicked: bluetoothConnector.connectAddress(deviceAddress)
                            }
                        }
                    }

                    ListView {
                        id: deviceListView
                        anchors.fill: parent
                        delegate: batteryDelegate
                        model: ListModel {}
                    }
                }
            }
        }
    }
}
