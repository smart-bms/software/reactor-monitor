import QtQuick 2.0

Item {
    property int buttonSize: uiFontSizeNormal * 2
    property int buttonCount: list.count
    property int buttonSpacing: list.spacing

    id: root

    Text {
        id: text
        font.pointSize: uiFontSizeBig
        text: upLabel
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: uiMargins
        color: brandColor02
    }

    Component {
        id: buttonDelegate

        Rectangle {
            property bool clicked: batteryModel.slimCapacity === parseInt(modelData)

            id: button
            width: root.buttonSize
            height: width
            opacity: area.pressed ? 0.6 : 1.0
            radius: uiMargins
            color: button.clicked ? brandColor02 : brandColor12

            Text {
                anchors.centerIn: parent
                text: modelData
                color: button.clicked ? brandColor12 : brandColor02
            }

            MouseArea {
                id: area
                anchors.fill: parent
                onClicked: deviceModel.updateSlimByCapacity(parseInt(modelData, 10))
            }
        }
    }

    ListView {
        id: list
        width: parent.width
        height: parent.height / 2
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.leftMargin: uiMargins
        spacing: uiMargins * 3
        orientation: ListView.Horizontal
        model: slimModel
        delegate: buttonDelegate      
    }
}
