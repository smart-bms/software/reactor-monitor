import QtQuick 2.0

Item {
    id: root

    property real vitalityWidth: width

    Text {
        id: vitalityText
        font.pointSize: uiFontSizeBig
        text: downLabel
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: uiMargins
        color: brandColor02
    }

    Rectangle {
        anchors.top: vitalityText.bottom
        anchors.topMargin: uiMargins
        width: vitalityWidth
        height: uiFontSizeNormal * 2
        radius: uiMargins
        color: brandColor02
        anchors.left: parent.left
        anchors.leftMargin: uiMargins

        Text {
            font.pointSize: uiFontSizeSmall
            text: batteryModel.slimVitality + "%"
            anchors.centerIn: parent
            color: brandColor12
        }
    }
}
