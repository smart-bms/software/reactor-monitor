import QtQuick 2.0

Item {
    property string upLabel: "Capacity"
    property string downLabel: "Vitality"

    id: root

    Column {
        anchors.centerIn: parent
        width: capacity.buttonSize * capacity.buttonCount + capacity.buttonSpacing * (capacity.buttonCount - 1)
        height: parent.height

        CapacityEntry {
            id: capacity
            height: parent.height / 2
            width: parent.width
        }

        VitalityEntry {
            id: vitality
            height: parent.height / 2
            width: parent.width
        }
    }
}
