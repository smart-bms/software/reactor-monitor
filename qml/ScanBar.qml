import QtQuick 2.0
import QtQuick.Controls 2.0

Item {
    property bool active: false

    id: root
    state: active ? "active" : "inactive"

    states: [
        State {
            name: "active"
            PropertyChanges {
                target: root
                y: 0
            }
        },
        State {
            name: "inactive"
            PropertyChanges {
                target: root
                y: -height
            }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation {
                target: root
                properties: "y"
                duration: 200
                easing.type: Easing.InOutQuad
            }
        }
    ]

    Rectangle {
        anchors.fill: parent
        anchors.margins: 5
        color: brandColor02
        radius: 5

        Row {
            anchors.centerIn: parent
            height: parent.height

            Text {
                anchors.verticalCenter: parent.verticalCenter
                text: "Scanning devices..."
                font.pointSize: parent.height * 0.3
                color: brandColor11
            }

            BusyIndicator {
                anchors.verticalCenter: parent.verticalCenter
                width: height
                height: parent.height * 0.8
                enabled: true
            }
        }
    }
}
