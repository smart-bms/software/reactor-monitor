#include "shellclient.h"
#include "bluetoothconnector.h"
#include "smart-bms.h"

static int crc8ccitt(int crc, int data)
{
    crc ^= data;

    for (int i = 0; i < 8; i++)
    {
        if (crc & 0x80)
            crc = (crc << 1) ^ 0x07;
        else
            crc = crc << 1;
    }

    return crc & 0xff;
}

ShellClient::ShellClient(BluetoothConnector *connector)
    : QObject(connector)
    , _connector(connector)
{
    connect(connector, &BluetoothConnector::readBytes,
            this, &ShellClient::readData);
}

BluetoothConnector *ShellClient::connector() const
{
    return _connector;
}

bool ShellClient::send(int code, const void *data, int size)
{
    quint8 gap[4], header[8], fuse[2];
    QByteArray bytes;
    int crc;

    for (size_t i = 0; i < sizeof(gap); i++)
        gap[i] = 0;

    crc = static_cast<quint8>(code);
    crc = crc8ccitt(crc, size);
    crc = crc8ccitt(crc, 0);
    crc = crc8ccitt(crc, 0);

    header[0] = 0xff;
    header[1] = 0x00;
    header[2] = static_cast<quint8>(code);
    header[3] = static_cast<quint8>(size);
    header[4] = 0;
    header[5] = 0;
    header[6] = 0xaa;
    header[7] = static_cast<quint8>(crc);

    for (auto i = 0; i < size; i++)
        crc = crc8ccitt(crc, reinterpret_cast<const quint8 *>(data)[i]);

    fuse[0] = static_cast<quint8>(crc);
    fuse[1] = 0x55;

    bytes.append(reinterpret_cast<const char *>(gap), sizeof(gap));
    bytes.append(reinterpret_cast<const char *>(header), sizeof(header));
    bytes.append(reinterpret_cast<const char *>(data), size);
    bytes.append(reinterpret_cast<const char *>(fuse), sizeof(fuse));

    return writeData(bytes);
}

bool ShellClient::sendAck()
{
    return send(COMMAND_ACK, nullptr, 0);
}

bool ShellClient::sendTick()
{
    return send(COMMAND_TICK, nullptr, 0);
}

bool ShellClient::sendSmartLimit(int index)
{
    quint8 byte = static_cast<quint8>(index);
    return send(COMMAND_SMART_LIMIT, reinterpret_cast<void *>(&byte), 1);
}

bool ShellClient::sendQuerySettings()
{
    return send(COMMAND_QUERY_SETTINGS, nullptr, 0);
}

bool ShellClient::sendQuerySmartLimit()
{
    return send(COMMAND_QUERY_SMART_LIMIT, nullptr, 0);
}

bool ShellClient::writeData(const QByteArray &bytes)
{
    if (connector())
        return connector()->writeBytes(bytes);

    return false;
}

void ShellClient::readData(const QByteArray &bytes)
{
    foreach (auto b, bytes)
        feedByte(static_cast<quint8>(b));
}

void ShellClient::feedByte(int byte)
{
    _cmdIndex++;

    switch (_cmdIndex)
    {
    case 1:
        if (byte != 0xff)
            _cmdIndex = 0;
        break;

    case 2:
        if (byte != 0x00)
            _cmdIndex = 0;
        break;

    case 3:
        _cmdCode = byte;
        _cmdCrc = byte;
        break;

    case 4:
        _cmdSize = byte;
        _cmdCrc = crc8ccitt(_cmdCrc, byte);
        break;

    case 5:
        _cmdCrc = crc8ccitt(_cmdCrc, byte);
        break;

    case 6:
        _cmdCrc = crc8ccitt(_cmdCrc, byte);
        break;

    case 7:
        if (byte != 0xaa)
            _cmdIndex = 0;
        break;

    case 8:
        if (byte != _cmdCrc)
            _cmdIndex = 0;
        break;

    case 9:
        _cmdData.clear();
        [[fallthrough]];

    default:
        if (_cmdIndex < _cmdSize + 9)
        {
            _cmdData.append(static_cast<char>(byte));
            _cmdCrc = crc8ccitt(_cmdCrc, byte);
        }
        else if (_cmdIndex == _cmdSize + 9)
        {
            if (byte != _cmdCrc)
                _cmdIndex = 0;
        }
        else
        {
            if (byte == 0x55)
                readCommand();

            _cmdIndex = 0;
        }
    }
}

void ShellClient::readCommand()
{
    switch (_cmdCode)
    {
    case COMMAND_TOCK:
        qDebug() << "TOCK";
        break;

    case COMMAND_VOLTAGE:
        readVoltage();
        break;

    case COMMAND_CURRENT:
        readCurrent();
        break;

    case COMMAND_SETTINGS:
        settingsReceived(_cmdData);
        break;

    case COMMAND_SMART_LIMIT:
        readSmartLimit();
        break;
    }
}

void ShellClient::readVoltage()
{
    QVector<int> voltages;

    for (int i = 0; i < _cmdData.length() / 2; i++)
    {
        int low = static_cast<unsigned char>(_cmdData[i * 2]);
        int high = static_cast<unsigned char>(_cmdData[i * 2 + 1]);

        voltages.append(low | (high << 8));
    }

    voltageReceived(voltages);
}

void ShellClient::readCurrent()
{
    currentReceived(*reinterpret_cast<qint32 *>(_cmdData.data()));
}

void ShellClient::readSmartLimit()
{
    quint8 byte = *reinterpret_cast<quint8 *>(_cmdData.data());
    smartLimitReceived(byte);
}
