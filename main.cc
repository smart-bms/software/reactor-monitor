#include "bluetoothconnector.h"
#include "celllistmodel.h"
#include "devicemodel.h"
#include "batterymodel.h"
#include "settingsmodel.h"
#include "shellclient.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QStringList>
#include <QQmlContext>
#include <QDebug>

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    auto ctx = engine.rootContext();

    auto device = new DeviceModel(ctx);
    auto connector = device->connector();
    auto settings = device->settings();
    auto battery = device->battery();
    auto cellList = battery->cellList();

    ctx->setContextProperty("slimModel", QVariant::fromValue(QStringList()));

    QObject::connect(device, &DeviceModel::slimModelChanged,
                     [ctx](QStringList model) {
        ctx->setContextProperty("slimModel", QVariant::fromValue(model));
    });

    ctx->setContextProperty("bluetoothConnector", connector);
    ctx->setContextProperty("deviceModel", device);
    ctx->setContextProperty("cellListModel", cellList);
    ctx->setContextProperty("batteryModel", battery);
    ctx->setContextProperty("settingsModel", settings);

    ctx->setContextProperty("brandColor01", "#f7f7f7");
    ctx->setContextProperty("brandColor02", "#00cf9f");
    ctx->setContextProperty("brandColor03", "#00f7aa");
    ctx->setContextProperty("brandColor11", "#1e1e1e");
    ctx->setContextProperty("brandColor12", "#000000");
    ctx->setContextProperty("brandColor04", "#1e1e1e");
    ctx->setContextProperty("brandColor05", "#de5044");

    ctx->setContextProperty("uiMargins", 5);
    ctx->setContextProperty("uiFontSizeBig", 20);
    ctx->setContextProperty("uiFontSizeNormal", 18);
    ctx->setContextProperty("uiFontSizeSmall", 16);
    ctx->setContextProperty("uiBarWidth", 12);
    ctx->setContextProperty("uiAnimationTimeNormal", 300);
    ctx->setContextProperty("uiAnimationTimeLong", 1000);

    engine.load(QUrl("qrc:/qml/Application.qml"));

    return app.exec();
}
