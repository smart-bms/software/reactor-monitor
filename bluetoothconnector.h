#ifndef BLUETOOTHCONNECTOR_H
#define BLUETOOTHCONNECTOR_H

#include <QObject>
#include <QTimer>
#include <QSharedPointer>
#include <QLowEnergyController>
#include <QBluetoothDeviceDiscoveryAgent>

class BluetoothConnector : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool connecting
               READ connecting
               NOTIFY connectingChanged)
    Q_PROPERTY(bool connected
               READ connected
               NOTIFY connectedChanged)
    Q_PROPERTY(bool scanEnabled
               READ scanEnabled
               WRITE setScanEnabled
               NOTIFY scanEnabledChanged)
    Q_PROPERTY(bool reconnect
               READ reconnect
               WRITE setReconnect
               NOTIFY reconnectChanged)
    Q_PROPERTY(QString deviceName
               READ deviceName
               NOTIFY deviceNameChanged)
    Q_PROPERTY(quint64 deviceAddress
               READ deviceAddress
               NOTIFY deviceAddressChanged)
    Q_PROPERTY(QString foundDeviceName
               READ foundDeviceName)
    Q_PROPERTY(quint64 foundDeviceAddress
               READ foundDeviceAddress)

public:
    explicit BluetoothConnector(QObject *parent = nullptr);
    ~BluetoothConnector();

    bool connecting();
    bool connected();
    bool scanEnabled();
    bool reconnect();
    QString deviceName();
    quint64 deviceAddress();
    QString foundDeviceName();
    quint64 foundDeviceAddress();

signals:
    void connectingChanged(bool);
    void connectedChanged(bool);
    void deviceNameChanged(QString);
    void deviceAddressChanged(quint64);
    void scanEnabledChanged(bool);
    void reconnectChanged(bool);
    void deviceAdded();
    void deviceRemoved();
    void deviceListCleared();
    void deviceDisconnected();
    void deviceConnecting();
    void deviceConnected();
    void readBytes(const QByteArray &);

public slots:
    void setScanEnabled(bool value);
    void setReconnect(bool value);
    void connectAddress(quint64 address);
    void disconnect();
    void disconnectByUser();
    bool writeBytes(QByteArray bytes);
    QString addressToString(quint64);

private slots:
    void remoteDeviceFound(const QBluetoothDeviceInfo &info);
    void scanFinished();
    void serviceDiscoveryFinished();
    void leError(const QLowEnergyController::Error &error);
    void serviceStateChanged(const QLowEnergyService::ServiceState &state);
    void deviceFullyConnected();
    void readCharacteristic(const QLowEnergyCharacteristic &, const QByteArray &);
    void connectingTimeout();

private:
    QTimer _timer;
    bool _connecting;
    bool _connected;
    bool _scanEnabled;
    bool _reconnect;
    QString _deviceName;
    quint64 _deviceAddress;
    QString _foundDeviceName;
    quint64 _foundDeviceAddress;
    QMap<quint64, QBluetoothDeviceInfo> _infoMap;
    QBluetoothDeviceDiscoveryAgent *_discoveryAgent;
    QLowEnergyController *_leController;
    QLowEnergyService *_leService;
    QLowEnergyCharacteristic _leCharacteristic;
    QLowEnergyDescriptor _leDescriptor;
};

#endif // BLUETOOTHCONNECTOR_H
