#include "devicemodel.h"
#include "bluetoothconnector.h"
#include "settingsmodel.h"
#include "batterymodel.h"
#include "celllistmodel.h"
#include "shellclient.h"

#include <QDebug>

DeviceModel::DeviceModel(QObject *parent)
    : QObject(parent)
    , _connector(new BluetoothConnector(this))
    , _settings(new SettingsModel(this))
    , _battery(new BatteryModel(settings()))
    , _shell(new ShellClient(connector()))
    , _connected(false)
    , _dirtySlim(true)
    , _smartLimit(0)
{
    _ackTimer.setInterval(250);
    _ackTimer.setSingleShot(false);
    _ackTimer.start();

    _slimTimer.setInterval(4000);
    _slimTimer.setSingleShot(false);
    _slimTimer.start();

    connect(&_ackTimer, &QTimer::timeout,
            this, &DeviceModel::timerTick);
    connect(&_slimTimer, &QTimer::timeout,
            [this]() {
        _dirtySlim = true;
    });
    connect(connector(), &BluetoothConnector::connectedChanged,
            this, &DeviceModel::setConnected);
    connect(connector(), &BluetoothConnector::deviceDisconnected,
            settings(), &SettingsModel::clear);
    connect(shell(), &ShellClient::settingsReceived,
            this, &DeviceModel::receiveSettings);
    connect(shell(), &ShellClient::voltageReceived,
            battery(), &BatteryModel::updateVoltages);
    connect(shell(), &ShellClient::smartLimitReceived,
            this, &DeviceModel::receiveSmartLimit);
    connect(settings(), &SettingsModel::cellCountChanged,
            battery(), &BatteryModel::setCellCount);
    connect(settings(), &SettingsModel::loadedChanged,
            this, &DeviceModel::updateSlimModel);
    connect(shell(), &ShellClient::currentReceived,
            [this](qint32 value) {
        battery()->setDischargingCurrent(qMax(0, value));
        battery()->setChargingCurrent(qMax(0, -value));
    });

}

DeviceModel::~DeviceModel()
{
    qDebug() << "destroy DeviceModel";
}

BluetoothConnector *DeviceModel::connector()
{
    return _connector;
}

SettingsModel *DeviceModel::settings()
{
    return _settings;
}

BatteryModel *DeviceModel::battery()
{
    return _battery;
}

ShellClient *DeviceModel::shell()
{
    return _shell;
}

bool DeviceModel::connected()
{
    return _connected;
}

int DeviceModel::smartLimit()
{
    return _smartLimit;
}

QStringList DeviceModel::slimModel() const
{
    return _slimModel;
}

void DeviceModel::updateSlimByCapacity(int capacity)
{
    for (int i = 0; i < settings()->slimCount(); i++)
        if (settings()->slimCapacity(i) == capacity)
        {
            _dirtySlim = true;
            setSmartLimit(i);
            shell()->sendSmartLimit(i);
            break;
        }
}

void DeviceModel::setConnected(bool value)
{
    if (value != connected())
    {
        _connected = value;
        connectedChanged(value);
        _ackTimer.setInterval(value ? 1000 : 250);
    }
}

void DeviceModel::setSmartLimit(int value)
{
    if (value != _smartLimit)
    {
        _smartLimit = value;
        smartLimitChanged(value);
    }

    battery()->setSlimCapacity(settings()->slimCapacity(value));
    battery()->setSlimVitality(settings()->slimVitality(value));
}

void DeviceModel::timerTick()
{
    if (not settings()->loaded())
    {
        shell()->sendQuerySettings();
    }
    else if (_dirtySlim)
    {
        shell()->sendQuerySmartLimit();
    }
    else
    {
        shell()->sendAck();
    }
}

void DeviceModel::receiveSettings(QByteArray bytes)
{
    settings()->load(bytes);
}

void DeviceModel::receiveVoltages(QVector<int> voltages)
{
    battery()->updateVoltages(voltages);
}

void DeviceModel::updateSlimModel()
{
    QStringList model;

    for (int i = 0; i < settings()->slimCount(); i++)
        model.push_back(QString::number(settings()->slimCapacity(i)));

    _slimModel = model;
    slimModelChanged(model);
}

void DeviceModel::receiveSmartLimit(int value)
{
    _dirtySlim = false;
    setSmartLimit(value);
}
