#include "batterymodel.h"
#include "celllistmodel.h"
#include "settingsmodel.h"

#include <QDebug>

BatteryModel::BatteryModel(SettingsModel *settings)
    : QObject(settings)
    , _settings(settings)
    , _cellList(new CellListModel(this))
    , _cellCount(0)
    , _voltage(0)
    , _temp0(0)
    , _slimCap(0)
    , _slimVit(0)
    , _chargingCurrent(0)
    , _dischargingCurrent(0)
{
    connect(this, &BatteryModel::cellCountChanged,
            cellList(), &CellListModel::setCount);
    connect(this, &BatteryModel::slimCapacityChanged,
            this, &BatteryModel::setSlimVitality);
}

CellListModel *BatteryModel::cellList() const
{
    return _cellList;
}

SettingsModel *BatteryModel::settings() const
{
    return _settings;
}

int BatteryModel::cellCount() const
{
    return _cellCount;
}

int BatteryModel::voltage() const
{
    return _voltage;
}

int BatteryModel::temperature() const
{
    return _temp0;
}

int BatteryModel::minCell() const
{
    return _minCell;
}

int BatteryModel::maxCell() const
{
    return _maxCell;
}

int BatteryModel::slimCapacity() const
{
    return _slimCap;
}

int BatteryModel::slimVitality() const
{
    return _slimVit;
}

int BatteryModel::chargingCurrent() const
{
    return _chargingCurrent;
}

int BatteryModel::dischargingCurrent() const
{;
    return _dischargingCurrent;
}

float BatteryModel::voltageToLevel(int volt) const
{
    auto upperIndex = 1;

    while (upperIndex < CAP_VOLEND and settings()->capVol(upperIndex) <= volt)
        upperIndex++;

    auto lowerIndex = upperIndex - 1;

    auto lower = settings()->capVol(lowerIndex);
    auto upper = settings()->capVol(upperIndex);

    auto level = static_cast<float>(volt - lower) / (upper - lower);

    level *= 0.2f;
    level += lowerIndex * 0.2f;

    return std::clamp(level, 0.f, 1.f);
}

float BatteryModel::temperatureToLevel(int temp) const
{
    auto level = static_cast<float>(temp + 20) / (60 + 20);

    return std::clamp(level, 0.f, 1.f);
}

void BatteryModel::updateVoltages(QVector<int> voltages)
{
    cellList()->updateVoltages(voltages);

    int sum = 0;
    int min = INT_MAX;
    int max = INT_MIN;

    for (auto v : voltages)
    {
        sum += v;
        min = std::min(min, v);
        max = std::max(max, v);
    }

    setVoltage(sum);
    setMinCell(min);
    setMaxCell(max);
}

void BatteryModel::setCellCount(int value)
{
    if (value != _cellCount)
    {
        _cellCount = value;
        cellCountChanged(value);
    }
}

void BatteryModel::setVoltage(int value)
{
    if (value != _voltage)
    {
        _voltage = value;
        voltageChanged(value);
    }
}

void BatteryModel::setMinCell(int value)
{
    if (value != minCell())
    {
        _minCell = value;
        minCellChanged(value);
    }
}

void BatteryModel::setMaxCell(int value)
{
    if (value != maxCell())
    {
        _maxCell = value;
        maxCellChanged(value);
    }
}

void BatteryModel::setTemperature(int value)
{
    if (value != _temp0)
    {
        _temp0 = value;
        temperatureChanged(value);
    }
}

void BatteryModel::setSlimCapacity(int value)
{
    if (value != _slimCap)
    {
        _slimCap = value;
        slimCapacityChanged(value);
    }
}

void BatteryModel::setSlimVitality(int value)
{
    if (value != _slimVit)
    {
        _slimVit = value;
        slimVitalityChanged(value);
    }
}

void BatteryModel::setChargingCurrent(int value)
{
    if (value != _chargingCurrent)
    {
        _chargingCurrent = value;
        chargingCurrentChanged(value);
    }

}

void BatteryModel::setDischargingCurrent(int value)
{
    if (value != _dischargingCurrent)
    {
        _dischargingCurrent = value;
        dischargingCurrentChanged(value);
    }
}
