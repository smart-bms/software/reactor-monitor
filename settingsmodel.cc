#include "settingsmodel.h"

#include <QDebug>

SettingsModel::SettingsModel(QObject *parent)
    : QObject(parent)
    , _cellCount(0)
    , _slimCount(0)
    , _vol0(0)
    , _vol20(0)
    , _vol40(0)
    , _vol60(0)
    , _vol80(0)
    , _vol100(0)
    , _ovp(0)
    , _ovpRelease(0)
    , _uvp(0)
    , _uvpRelease(0)
    , _chgOcp(0)
    , _dsgOcp(0)
    , _dsgOtp(0)
    , _dsgUtp(0)
    , _loaded(false)
{

}

bool SettingsModel::load(const QByteArray &bytes)
{
    const settings_data *data = reinterpret_cast<const settings_data *>(bytes.data());

    setCellCount(data->cell_count);
    _slimCount = data->smart_limit_count;
    slimCountChanged(_slimCount);

    for (int i = 0; i < CAP_VOLEND; i++)
        _capVol[i] = data->cap_vol[i];

    for (int i = 0; i < MAX_SMART_LIMIT_COUNT; i++)
    {
        _slimCapacity[i] = data->slim_cap[i];
        _slimVitality[i] = data->slim_vit[i];
    }

    setVol0(data->cap_vol[CAP_VOL0]);
    setVol20(data->cap_vol[CAP_VOL20]);
    setVol40(data->cap_vol[CAP_VOL40]);
    setVol60(data->cap_vol[CAP_VOL60]);
    setVol80(data->cap_vol[CAP_VOL80]);
    setVol100(data->cap_vol[CAP_VOL100]);

    _ovp = data->cell_ovp;
    _ovpRelease = data->cell_ovp_release;
    _uvp = data->cell_uvp;
    _uvpRelease = data->cell_uvp_release;
    _chgOcp = static_cast<int>(data->chg_ocp);
    _dsgOcp = static_cast<int>(data->dsg_ocp);
    _dsgOtp = static_cast<int>(data->dsg_otp);
    _dsgUtp = static_cast<int>(data->dsg_utp);
    ovpChanged(ovp());
    ovpReleaseChanged(ovpRelease());
    uvpChanged(uvp());
    uvpReleaseChanged(uvpRelease());
    chgOcpChanged(chgOcp());
    dsgOcpChanged(dsgOcp());
    dsgOtpChanged(dsgOtp());
    dsgUtpChanged(dsgUtp());

    _loaded = true;
    loadedChanged(true);
    return true;
}

void SettingsModel::clear()
{
    _loaded = false;
    loadedChanged(false);

    setCellCount(0);
    setVol0(0);
    setVol20(0);
    setVol40(0);
    setVol60(0);
    setVol80(0);
    setVol100(0);
}

int SettingsModel::capVol(int index) const
{
    return _capVol[index];
}

int SettingsModel::slimCapacity(int index) const
{
    return _slimCapacity[index];
}

int SettingsModel::slimVitality(int index) const
{
    return _slimVitality[index];
}

int SettingsModel::cellCount() const
{
    return _cellCount;
}

int SettingsModel::slimCount() const
{
    return _slimCount;
}

int SettingsModel::vol0() const
{
    return _vol0;
}

int SettingsModel::vol20() const
{
    return _vol20;
}

int SettingsModel::vol40() const
{
    return _vol40;
}

int SettingsModel::vol60() const
{
    return _vol60;
}

int SettingsModel::vol80() const
{
    return _vol80;
}

int SettingsModel::vol100() const
{
    return _vol100;
}

int SettingsModel::ovp() const
{
    return _ovp;
}

int SettingsModel::ovpRelease() const
{
    return _ovpRelease;
}

int SettingsModel::uvp() const
{
    return _uvp;
}

int SettingsModel::uvpRelease() const
{
    return _uvpRelease;
}

int SettingsModel::chgOcp() const
{
    return _chgOcp;
}

int SettingsModel::dsgOcp() const
{
    return _dsgOcp;
}

int SettingsModel::dsgOtp() const
{
    return _dsgOtp;
}

int SettingsModel::dsgUtp() const
{
    return _dsgUtp;
}

bool SettingsModel::loaded() const
{
    return _loaded;
}

void SettingsModel::setCellCount(int value)
{
    if (value != _cellCount)
    {
        _cellCount = value;
        cellCountChanged(value);
    }
}

void SettingsModel::setVol0(int value)
{
    if (value != vol0())
    {
        _vol0 = value;
        vol0Changed(value);
    }
}

void SettingsModel::setVol20(int value)
{
    if (value != vol20())
    {
        _vol20 = value;
        vol20Changed(value);
    }
}

void SettingsModel::setVol40(int value)
{
    if (value != vol40())
    {
        _vol40 = value;
        vol40Changed(value);
    }
}

void SettingsModel::setVol60(int value)
{
    if (value != vol60())
    {
        _vol60 = value;
        vol60Changed(value);
    }
}

void SettingsModel::setVol80(int value)
{
    if (value != vol80())
    {
        _vol80 = value;
        vol80Changed(value);
    }
}

void SettingsModel::setVol100(int value)
{
    if (value != vol100())
    {
        _vol100 = value;
        vol100Changed(value);
    }
}
