#ifndef SHELLCLIENT_H
#define SHELLCLIENT_H

#include <QObject>

class BluetoothConnector;

class ShellClient : public QObject
{
    Q_OBJECT
public:
    explicit ShellClient(BluetoothConnector *connector);

    BluetoothConnector *connector() const;

signals:
    void voltageReceived(QVector<int>);
    void currentReceived(qint32);
    void settingsReceived(QByteArray);
    void smartLimitReceived(int);

public slots:
    bool send(int code, const void *data, int size);
    bool sendAck();
    bool sendTick();
    bool sendSmartLimit(int index);
    bool sendQuerySettings();
    bool sendQuerySmartLimit();

private slots:
    bool writeData(const QByteArray &bytes);
    void readData(const QByteArray &bytes);
    void feedByte(int byte);
    void readCommand();
    void readVoltage();
    void readCurrent();
    void readSmartLimit();

private:
    BluetoothConnector *_connector;
    int _cmdIndex;
    int _cmdCode;
    int _cmdCrc;
    int _cmdSize;
    QByteArray _cmdData;
};

#endif // SHELLCLIENT_H
