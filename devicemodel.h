#ifndef DEVICEMODEL_H
#define DEVICEMODEL_H

#include <QObject>
#include <QTimer>

class BluetoothConnector;
class SettingsModel;
class BatteryModel;
class ShellClient;

class DeviceModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool connected
               READ connected
               WRITE setConnected
               NOTIFY connectedChanged)
    Q_PROPERTY(int smartLimit
               READ smartLimit
               WRITE setSmartLimit
               NOTIFY smartLimitChanged)
    Q_PROPERTY(QStringList slimModel
               READ slimModel
               NOTIFY slimModelChanged)

public:
    explicit DeviceModel(QObject *parent = nullptr);
    ~DeviceModel();

    BluetoothConnector *connector();
    SettingsModel *settings();
    BatteryModel *battery();
    ShellClient *shell();
    bool connected();
    int smartLimit();
    QStringList slimModel() const;

signals:
    void connectedChanged(bool);
    void smartLimitChanged(int);
    void slimModelChanged(QStringList);

public slots:
    void updateSlimByCapacity(int capacity);
    void setConnected(bool value);
    void setSmartLimit(int value);

private slots:
    void timerTick();
    void receiveSettings(QByteArray);
    void receiveVoltages(QVector<int>);
    void updateSlimModel();
    void receiveSmartLimit(int);

private:
    BluetoothConnector *_connector;
    SettingsModel *_settings;
    BatteryModel *_battery;
    ShellClient *_shell;
    QTimer _ackTimer;
    QTimer _slimTimer;
    bool _connected;
    bool _dirtySlim;
    int _smartLimit;
    QStringList _slimModel;
};

#endif // DEVICEMODEL_H
